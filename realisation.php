<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" >
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
	<meta http-equiv="Content-Language" content="fr"/>
	<meta http-equiv="refresh" content="60"/>
	<title>Othentic</title>
	<link href="styles/realisation.css" media="all" rel="stylesheet" type="text/css" />
	<link rel="icon" type="image/png" href="images/logo.png"/>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>
</head>
<body>
	<div id="site">
	<h1><img src="images/othentic1.jpg" alt="Othentic"/></h1>
	<p id="baro">_________________________________________________________________________________</p>

		<div id="banner">
		<div id="ongl"> <span id="acceuil"><a href="?page=acceuil">Acceuil</a></span> <span id="realisation"><a href="?page=realisation">Réalisation</a></span> <span id="contact"><a href="?page=contact">Contact</a></span>
		</div>
	</div>
	<div id="main">
		<h2>Exposition listant les réalisations faites par Othentic:</h2>
		<div id="menu">
			<ol>
				<li><a href="realisation.php#ocgd">Les Opérations Commerciales de la Grande Distribution évènementiel de Manifestations:</a></li>
				<li><a href="realisation.php#foire">Foire aux vins:</a></li>
				<li><a href="realisation.php#no">Noel</a></li>
				<li><a href="realisation.php#via">Fête de la viande:</a></li>
				<li><a href="realisation.php#fraich">Fraîche attitude:</a></li>
				<li><a href="realisation.php#fag">Foire aux Gras:</a></li>
				<li><a href="realisation.php#fap">Foire aux pontalons</a></li>
				<li><a href="realisation.php#plage">La Plage:</a></li>
				<li><a href="realisation.php#Boulanger">Fete de la Boulangerie:</a></li>
				<li><a href="realisation.php#poisson">La Poissonerie:</a></li>
				<li><a href="realisation.php#lbp">Le Blanc et la Puériculture</a></li>
				<li><a href="realisation.php#gibier">Le Gibier:</a></li>
				<li><a href="realisation.php#exo">Exotique:</a></li>
				<li><a href="realisation.php#evene">Evènementiel manifestations:</a></li>
				<li><a href="realisation.php#bain">Bains de Noel à Arcachon:</a></li>
				<li><a href="realisation.php#fcf">Festival du Cinéma Féminin au Palais des Congrès d’Arcachon:</a></li>
				<li><a href="realisation.php#jp">Journées du Patrimoine au Parc Mauresque à Arcachon:</a></li>
				<li><a href="realisation.php#galamiss">Gala Miss France au Palais des Congrès de Soulac sur mer:</a></li>
				<li><a href="realisation.php#vhdm">Vitrine Hermès dans un magasin Beauty Succes:</a></li>
				<li><a href="realisation.php#espaceguiguette">Espace Guinguette à la Foire Internationale de Bordeaux:</a></li>
				<li><a href="realisation.php#pmariage">Présentation des huitres lors d’un mariage:</a></li>
				<li><a href="realisation.php#soirvenise">Soirée Venise au Château Giscours:</a></li>
				<li><a href="realisation.php#mairie">Arbre de Noël à la Mairie de Bordeaux</a></li>
				<li><a href="realisation.php#vin">Vinexpo: STAND CALVET 2003</a></li>
				<li><a href="realisation.php#stclub">Stand CLUB de la CUB à la cité mondiale du vin:</a></li>
				<li><a href="realisation.php#annee6-7">Theâtralisation des années 60-70:</a></li>
				<li><a href="realisation.php#thalo">Theâtralisation d'Halloween:</a></li>
				<li><a href="realisation.php#trdclasse">Theâtralisation des rentrées de classes:</a></li>
			</ol>	
		</div>

		<h3 id="ocgd">Les Opérations Commerciales de la Grande Distribution évènementiel de Manifestations: </h3>		
		
		<p id="barr">______________________________________________________________________________________________</p>
		<span id="foire" > Foire aux vins:</span>	

		<img id="fav086" src="images/event/FAV/086.jpg" alt="086"/>
		<img id="fav093" src="images/event/FAV/093.jpg" alt="093"/>
		<img id="fav031" src="images/event/FAV/031.jpg" alt="031"/>
		<img id="fav029" src="images/event/FAV/029.jpg" alt="029"/>
		<img id="fav091" src="images/event/FAV/091.jpg" alt="091"/>
		<img id="fav125" src="images/event/FAV/vin.jpg" alt="vin"/>
		<img id="fav126" src="images/event/FAV/vino.jpg" alt="vino"/>
		<img id="fav127" src="images/event/FAV/127.jpg" alt="127"/>
		<p id="barro">______________________________________________________________________________________________</p>

		<span id="no">Noel: </span>
		<img id="no32" src="images/event/noel/32.jpg" alt="32"/>
		<img id="no033" src="images/event/noel/033.jpg" alt="033"/>
		<img id="no042" src="images/event/noel/042.jpg" alt="042"/>
		<img id="no012" src="images/event/noel/012.jpg" alt="012"/>
		<img id="no5" src="images/event/noel/5.jpg" alt="5"/>
		<img id="no6" src="images/event/noel/AUCBouliac.jpg" alt="AUC Bouliac 2"/>
		<img id="no7" src="images/event/noel/chateau.jpg" alt="chateau"/>
		<img id="no8" src="images/event/noel/papanoel.jpg" alt="papanoel"/>
		<p id="barrr">______________________________________________________________________________________________</p>

		<span id="via">Fête de la viande:</span>

		<img id="bouch130" src="images/event/boucherie/130.jpg" alt="130"/>
		<img id="bouch129" src="images/event/boucherie/129.jpg" alt="129"/>
		<img id="bouch124" src="images/event/boucherie/124.jpg" alt="124"/>
		<img id="bouch030" src="images/event/boucherie/030.jpg" alt="030"/>
		<img id="bouch043" src="images/event/boucherie/043.jpg" alt="043"/>
		<img id="bouch053" src="images/event/boucherie/053.jpg" alt="053"/>
		<img id="bouch113" src="images/event/boucherie/113.jpg" alt="113"/>
		<img id="bouch123" src="images/event/boucherie/123.jpg" alt="123"/>

		<p id="barrro">______________________________________________________________________________________________</p>

		<span id="fraich">Fraîche attitude:</span>

		<img id="fleg075" src="images/event/fleg/075.jpg" alt="075"/>
		<img id="fleg106" src="images/event/fleg/106.jpg" alt="106"/>
		<img id="fleg085" src="images/event/fleg/085.jpg" alt="085"/>
		<img id="fleg121" src="images/event/fleg/121.jpg" alt="121"/>
		<img id="fleg111" src="images/event/fleg/111.jpg" alt="111"/>
		<img id="fleg119" src="images/event/fleg/119.jpg" alt="119"/>
		<img id="fleg130" src="images/event/fleg/130.jpg" alt="130"/>
		<img id="fleg132" src="images/event/fleg/132.jpg" alt="132"/>
		<img id="fleg133" src="images/event/fleg/133.jpg" alt="133"/>
		<img id="fleg135" src="images/event/fleg/135.jpg" alt="135"/>
		<p id="barrrr">______________________________________________________________________________________________</p>

		<span id="fag">Foire aux Gras:</span>

		<img id="fag1" src="images/event/FAGras/Béziers.jpg" alt="Béziers"/>
		<img id="fag2" src="images/event/FAGras/Fogras 2.jpg" alt="Fogras 2"/>
		<img id="fag3" src="images/event/FAGras/LECMarmande.jpg" alt="LECMarmande"/>
		<img id="fag4" src="images/event/FAGras/Marsac 4.jpg" alt="Marsac4"/>
		<p id="barrrrrrrrrr">______________________________________________________________________________________________</p>

		<span id="fap">Foire aux pontalons</span>

		<img id="fap1" src="images/event/FAP/Annemasse 2.jpg" alt="Annemasse 2"/>
		<img id="fap2" src="images/event/FAP/Arbent Oyonnax 1.jpg" alt="Arbent Oyonnax 1"/>
		<img id="fap3" src="images/event/FAP/Genoble Fontaine 1.jpg" alt="Genoble Fontaine 1"/>
		<img id="fap4" src="images/event/FAP/Valence Deux 1.jpg" alt="Valence Deux 1"/>
		<img id="fap5" src="images/event/FAP/Valence Sud 2.jpg" alt="Valence Sud 2"/>
		<img id="fap6" src="images/event/FAP/IMG_8030.jpg" alt="IMG_8030"/>
		<p id="barrrrrrrrrro">______________________________________________________________________________________________</p>

		<span id="plage">La Plage:</span>

		<img id="plage1" src="images/event/plage/02CRFbègles3.jpg" alt="02CRFbègles3"/>
		<img id="plage2" src="images/event/plage/02CRFlormont2.jpg" alt="02CRFlormont2"/>
		<img id="plage3" src="images/event/plage/DSCF0349.jpg" alt="DSCF0349"/>
		<img id="plage4" src="images/event/plage/DSCN0768.jpg" alt="DSCN0768"/>
		<img id="plage5" src="images/event/plage/embruns autan.jpg" alt="embruns autan"/>
		<img id="plage6" src="images/event/plage/pieddansleau.jpg" alt="pieddansleau"/>
		<p id="barrrrrrrrrrrr">______________________________________________________________________________________________</p>

		<span id="Boulanger">Fete de la Boulangerie:</span>
		<img id="Boulanger1" src="images/event/boulangerie/031.jpg" alt="031"/>
		<img id="Boulanger2" src="images/event/boulangerie/032.jpg" alt="032"/>
		<img id="Boulanger3" src="images/event/boulangerie/DSCF0081.jpg" alt="DSCF0081"/>
		<img id="Boulanger4" src="images/event/boulangerie/DSCN0230.jpg" alt="DSCN0230"/>
		<p id="barrrrrrrrrrrro">______________________________________________________________________________________________</p>

		<span id="poissonerie">La Poissonerie: </span>
		<img id="poisson" src="images/event/poissonerie/barque.jpg" alt="Barque"/>
		<img id="poisson1" src="images/event/poissonerie/bzh.jpg" alt="bateau BZH 2"/>
		<img id="poisson2" src="images/event/poissonerie/huitres.jpg" alt="huitres"/>
		<img id="poisson3" src="images/event/poissonerie/marin.jpg" alt="Marin"/>
		<p id="barrrrrrrrrrrrr">______________________________________________________________________________________________</p>

		<span id="lbp">Le Blanc et la Puériculture</span>
		<img id="lbep" src="images/event/lbp/1Cb1.jpg" alt"1Cb1"/>
		<img id="lbep1" src="images/event/lbp/1Cb2.jpg" alt"1Cb2"/>
		<img id="lbep2" src="images/event/lbp/1Cb3.jpg" alt"1Cb3"/>
		<img id="lbep3" src="images/event/lbp/C1.jpg" alt"C1"/>
		<img id="lbep4" src="images/event/lbp/C2.jpg" alt"C2"/>
		<img id="lbep5" src="images/event/lbp/C3.jpg" alt"C3"/>
		<img id="lbep6" src="images/event/lbp/C4.jpg" alt"C4"/>
		<img id="lbep7" src="images/event/lbp/DSC02681.jpg" alt"DSC02681"/>
		<p id="barrrrrrrrrrrrro">______________________________________________________________________________________________</p>

		<span id="gibier">Le Gibier:</span>
		<img id="gibier1" src="images/event/gibier/AUC Périgueux.jpg" alt="AUC Périgueux"/>
		<img id="gibier2" src="images/event/gibier/AUC Taverny.jpg" alt="AUC Taverny"/>
		<img id="gibier3" src="images/event/gibier/AUC Vélizy.jpg" alt="AUC Vélizy"/>
		<img id="gibier4" src="images/event/gibier/IMG_6765.jpg" alt="IMG_6765"/>
		<p id="barrrrrrrrrrrrrr">______________________________________________________________________________________________</p>

		<span id="exo"> Exotique:</span>
		<img id="exo1" src="images/event/exotique/107_0729.jpg" alt="107_0729" />
		<img id="exo2" src="images/event/exotique/DSCF0018.jpg" alt="DSCF0018" />
		<img id="exo3" src="images/event/exotique/DSCF0207.jpg" alt="DSCF0207" />
		<img id="exo4" src="images/event/exotique/Photo014.jpg" alt="Photo014" />
		<p id="barrrrrrrrrrrrrro">______________________________________________________________________________________________</p>

		<h4 id ="evene">Evènementiel manifestations: </h4>
		<span id="bain"> Bains de Noel à Arcachon</span>

		<img id="bain1" src="images/event/evenementiel/bain 1.jpg" alt="bain 1"/>
		<img id="bain2" src="images/event/evenementiel/bain 2.jpg" alt="bain 2"/>
		<p id="barrrrrrrrrrrrrrro">______________________________________________________________________________________________</p>

		<span id="fcf">Festival du Cinéma Féminin au Palais des Congrès d’Arcachon</span>
		<img id="cinema" src="images/event/evenementiel/cinema.jpg" alt="cinema"/>
		<p id="barrrrrrrrrrrrrrrr">______________________________________________________________________________________________</p>

		<span id="jp">Journées du Patrimoine au Parc Mauresque à Arcachon</span>

		<img id="jp1" src="images/event/evenementiel/jp 1.jpg" alt="jp 1"/>
		<img id="jp2" src="images/event/evenementiel/jp 2.jpg" alt="jp 2"/>
		<p id="barrrrrrrrrrrrrrrro">______________________________________________________________________________________________</p>

		<span id="galamiss">Gala Miss France au Palais des Congrès de Soulac sur mer</span>

		<img id="miss" src="images/event/evenementiel/miss.jpg" alt="miss"/>
		<p id="barrrrrrrrrrrrrrrrr">______________________________________________________________________________________________</p>

		<span id="vhdm">Vitrine Hermès dans un magasin Beauty Succes</span>

		<img id="BS1" src="images/event/evenementiel/BS 1.jpg" alt="BS 1"/>
		<img id="BS2" src="images/event/evenementiel/BS 2.jpg" alt="BS 2"/>
		<p id="barrrrrrrrrrrrrrrrro">______________________________________________________________________________________________</p>

		<span id="espaceguinguette">Espace Guinguette à la Foire Internationale de Bordeaux</span>

		<img id="guinguette" src="images/event/evenementiel/guinguette.jpg" alt="guinguette"/>
		<p id="barrrrrrrrrrrrrrrrrr">______________________________________________________________________________________________</p>


		<span id="pmariage">Présentation des huitres lors d’un mariage</span>
		<img id="mariage" src="images/event/evenementiel/mariage.jpg" alt="mariage"/>
		<p id="barrrrrrrrrrrrrrrrrro">______________________________________________________________________________________________</p>


		<span id="soirvenise">Soirée Venise au Château Giscours</span>
		<img id="venise" src="images/event/evenementiel/venise.jpg" alt="venise"/>
		<p id="barrrrrrrrrrrrrrrrrrr">______________________________________________________________________________________________</p>

		<span id="mairie">Arbre de Noël à la Mairie de Bordeaux</span>
		<img id="mairie1" src="images/event/evenementiel/mairie 1.jpg" alt="mairie 1"/>
		<img id="mairie2" src="images/event/evenementiel/mairie 2.jpg" alt="mairie 2"/>
		<p id="barrrrrrrrrrrrrrrrrrro">______________________________________________________________________________________________</p>

		<span id="vin">Vinexpo: STAND CALVET 2003</span>

		<img id="autrvinexpo1" src="images/event/autre/vinexpo1.jpg" alt="vinexpo1"/>
		<img id="autrvinexpo2" src="images/event/autre/vinexpo2.jpg" alt="vinexpo2"/>
		<img id="autrvinexpo3" src="images/event/autre/vinexpo3.jpg" alt="vinexpo3"/>
		<img id="autrvinexpo4" src="images/event/autre/vinexpo4.jpg" alt="vinexpo4"/>
		<img id="autrvinexpo5" src="images/event/evenementiel/calvet 1.jpg" alt="calvet 1"/>
		<img id="autrvinexpo6" src="images/event/evenementiel/calvet 2.jpg" alt="calvet 2"/>

		<p id="barrrrr">______________________________________________________________________________________________</p>


		<span id="stclub">Stand CLUB de la CUB à la cité mondiale du vin:</span>
		
		<img id="salonclub" src="images/event/autre/salon club.jpg" alt="salon club"/>
		<img id="salonclub1" src="images/event/autre/salon club1.jpg" alt="salon club1"/>

		<p id="barrrrro">______________________________________________________________________________________________</p>

		<span id="annee6-7">Theâtralisation des années 60-70:</span>

		<img id="annee60" src="images/event/autre/annee60.jpg" alt="annee60"/>
		<img id="annee60-1" src="images/event/autre/annee60-1.jpg" alt="annee60-1"/>
		<img id="annee60-2" src="images/event/autre/annee60-2.jpg" alt="annee60-2"/>
		<img id="annee70" src="images/event/autre/annee70.jpg" alt="annee70"/>
		<img id="annee70-1" src="images/event/autre/annee70-1.jpg" alt="annee70-1"/>
		<img id="annee70-2" src="images/event/autre/annee70-2.jpg" alt="annee70-2"/>

		<p id="barrrrrr">______________________________________________________________________________________________</p>

		<span id="thalo">Theâtralisation d'Halloween:</span>

		<img id="haloween" src="images/event/autre/halloween.jpg" alt="halloween"/>
		<img id="haloween1" src="images/event/autre/halloween1.jpg" alt="halloween1"/>
		<img id="haloween2" src="images/event/autre/halloween2.jpg" alt="halloween2"/>
		<img id="haloween3" src="images/event/autre/halloween3.jpg" alt="halloween3"/>

		<p id="barrrrrro">______________________________________________________________________________________________</p>

		<span id="trdclasse">Theâtralisation des rentrées de classes:</span>
		<img id="rdclasse" src="images/event/autre/rdclasse.jpg" alt="rdclasse"/>
		<img id="rdclasse1" src="images/event/autre/rdclasse1.jpg" alt="rdclasse1"/>
		<img id="rdclasse2" src="images/event/autre/rdclasse2.jpg" alt="rdclasse2"/>
		<img id="rdclasse3" src="images/event/autre/rdclasse3.jpg" alt="rdclasse3"/>
		<img id="rdclasse4" src="images/event/autre/rdclasse4.jpg" alt="rdclasse4"/>
		<img id="rdclasse-g" src="images/event/autre/rdclasse-g.jpg" alt="rdclasse-g"/>
		<img id="rdclasse-g1" src="images/event/autre/rdclasse-g1.jpg" alt="rdclasse-g1"/>
		<img id="rdclasse-g2" src="images/event/autre/rdclasse-g2.jpg" alt="rdclasse-g2"/>
		<img id="rdclasse-g3" src="images/event/autre/rdclasse-g3.jpg" alt="rdclasse-g3"/>
		<img id="rdclasse-g4" src="images/event/autre/rdclasse-g4.jpg" alt="rdclasse-g4"/>
	</div>
	<p id="barrrrrrr">_________________________________________________________________________________</p>
	<div id="footer">
		<p id="barrrrrrro">_________________________________________________________________________________</p>
		<p id="social">
		<span id="facebook"><img src="images/facebook_left.png" alt="facebook"/></span>  <span id="twitter"><img src="images/twitter.png" alt="twitter"/></span>  <span id="gplus"><img src="images/gplus.png" alt="google +"/></span>  <span id="youtube"><img src="images/youtube_left.png" alt="Youtube"/></span>
	</p>
	</div>
	</div>
	<script type="text/javascript" src="scripts/app.js"></script>
	<script type="text/javascript" src="scripts/jquery.min.js"></script>
</body>

</html>