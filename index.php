<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" >
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
	<meta http-equiv="Content-Language" content="fr"/>
	<meta name="description" content="Othentic, entreprise spécialisée dans la décoration et installation de tout évènement à thème, dans la vente et location de materiels de decorations et dans la théâtralisation."/>
	<meta name="keywords" content="location, décoration, théâtre, authentique, évènement, installation, mise en scène, matériel, vente, promotion"/>
	<meta name="author" content="Othentic"/>
	<meta http-equiv="refresh" content="60">
	<meta name="viewport" content="width=device-width, target-densitydpi=device-dpi"/>
	<Meta name = "dc.creator" content = "Othentic" />
 	<Meta name = "dc.language" content = "fr" />
 	<Meta name = "dc.creator" content = "Ollivier Caussade" />
 	<Meta name = "dc.date" content = "01/01/1998" />
 	<Meta name = "dc.description" content = "Othentic, entreprise spécialisée dans la décoration et installation de tout évènement à thème, dans la vente et location de materiels de decorations et dans la théâtralisation. C'est aussi une entreprise de location de matériel authentique pour la théâtralisation de l'espace de vente ou la décoration évènementielle. Othentic valorise les opérations marketing par une mise en scène originale qui permet d'augmenter ainsi les ventes." />
 	<Meta name = "dc.publisher" content = "Othentic" />
 	<Meta name = "dc.subject" content = "Théâtralisation, location, ventes, promotion, évènement, mise en scéne, décoration, matériel authentique, entreprise de location, installation, service." />
 	<Meta name = "dc.title" content = "Othentic: La théâtralisation de l'Espace de vente à thème." />
	<title>Othentic</title>
	<link href="styles/index.css" media="all" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" media="max_width" href="styles/mobile.css"/>
	<link rel="icon" type="image/png" href="images/logo.png"/>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>
</head>
<body>
	<p id="baro">_________________________________________________________________________________</p>
	<div id="banner">
		<div id="onglet"> <span id="acceuil"><a href="?page=acceuil">Acceuil</a></span> <span id="realisation"><a href="?page=realisation">Réalisation</a></span> <span id="contact"><a href="?page=contact">Contact</a></span>
		</div>
	</div>
	
	<?php
		$page = array("?page=realisation" => "realisation",
			"?page=acceuil" => "acceuil",
			"?page=contact" => "contact"


						);
	?>
	<?php 
		if (empty($_GET["page"])){
			$_GET["page"]=$page;
			include ('acceuil.php');
		}
		else 
		{
			$page=$_GET["page"];
			include ($page.".php");
		}
			
	?>
<script type="text/javascript" src="scripts/unslider.js"></script>
<script type="text/javascript" src="scripts/unslider.min.js"></script>
</body>
</html>