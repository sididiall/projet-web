<?php
    if (isset($_GET["value"]))
    {
        $value = $_GET["value"];
        if ($value == true)
        {
            $message = "Votre message a bien ete transmis, nous vous recontactons dans les meilleurs delais.";
        }
        else
        {
            $message = "Votre message n'a pas ete transmi, veuillez verifier vos donnees et reessayer.";
        }
    }
    else
    {
        $message ="";
    }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" >
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <meta http-equiv="Content-Language" content="fr">
	<title>Othentic</title>
	<link href="styles/contact.css" media="all" rel="stylesheet" type="text/css" />
    <link rel="icon" type="image/png" href="images/logo.png"/>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>
</head>
<body>
    <div id="site">
    <h1><img src="images/othentic1.jpg" alt="Othentic"/></h1>
        <div id="banner">
        <div id="ong"> <span id="acceuil"><a href="?page=acceuil">Acceuil</a></span> <span id="realisation"><a href="?page=realisation">Réalisation</a></span> <span id="contact"><a href="?page=contact">Contact</a></span>
        </div>
    </div>
    <h2 id="demande">Pour toutes demandes ou questions concernant la mise en place d’un projet, merci de remplir le formulaire ci-dessous.</h2>

    <div id="main">
        <form id="myForm" action="sendmail.php" method="post">
      <fieldset>
        <legend>Informations</legend>

        <label for="Prénom">Prénom:* </label>
        <input type="text" name="prenom" required autofocus />
        <br/>
  
        <label for="Nom">Nom:* </label>
        <input type="text" name="nom" required />
        <br/>
  
        <label for="email">Votre Email:* </label>
        <input type="text" name="email" required />
        <br/>

        <label for="telephone">Votre télèphone:* </label>
        <input type="text" name="telephone" required />
        <br/>

        <label for="statut">Statut: </label>
        <select name="statut" required>
            <option value="particulier">Particulier</option>
            <option value="professionel">Professionel</option>
        </select>
        <br/>
      </fieldset>
    <div id="mes">
        <p>
        <label for="comments">Message </label>
        <br/>
        <textarea name="message" cols="60" rows="20" required >
          Ecrivez votre message ici !
        </textarea>
        <br/>
        <input type="submit" value="Envoyez" />
        <input type="reset" value="Reset" />
      </p>
      <p>Tous les champs * sont obligatoires. <br/>
        info@othentic.fr<br/>
        Portable : 06 14 96 03 93 <br/>
        </p>
    </div>
 
    </form>
    </div>
     <div id="footer">
        <p id="baro">_________________________________________________________________________________</p>
        <p id="social">
        <span id="facebook"><img src="images/facebook_left.png" alt="facebook"/></span>  <span id="twitter"><img src="images/twitter.png" alt="twitter"/></span>  <span id="gplus"><img src="images/gplus.png" alt="google +"/></span>  <span id="youtube"><img src="images/youtube_left.png" alt="Youtube"/></span>
       </p> <br/>
        <p id="copy">Copyright WebDev &#169; 2014
        </p>
    </div>
    </div>
	
    <script type="text/javascript" src="scripts/app.js"></script>
    <script type="text/javascript" src="scripts/jquery.min.js"></script>
</body>
	
</html>