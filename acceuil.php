<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" >
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
	<meta http-equiv="Content-Language" content="fr"/>
	<meta name="description" content="Othentic, entreprise spécialisée dans la décoration et installation de tout évènement à thème, dans la vente et location de materiels de decorations et dans la théâtralisation."/>
	<meta name="keywords" content="location, décoration, théâtre, authentique, évènement, installation, mise en scène, matériel, vente, promotion"/>
	<meta name="author" content="Othentic"/>
	<meta http-equiv="refresh" content="60">
	<meta name="viewport" content="width=device-width, target-densitydpi=device-dpi"/>
	<Meta name = "dc.creator" content = "Othentic" />
 	<Meta name = "dc.language" content = "fr" />
 	<Meta name = "dc.creator" content = "Ollivier Caussade" />
 	<Meta name = "dc.date" content = "01/01/1998" />
 	<Meta name = "dc.description" content = "Othentic, entreprise spécialisée dans la décoration et installation de tout évènement à thème, dans la vente et location de materiels de decorations et dans la théâtralisation. C'est aussi une entreprise de location de matériel authentique pour la théâtralisation de l'espace de vente ou la décoration évènementielle. Othentic valorise les opérations marketing par une mise en scène originale qui permet d'augmenter ainsi les ventes." />
 	<Meta name = "dc.publisher" content = "Othentic" />
 	<Meta name = "dc.subject" content = "Théâtralisation, location, ventes, promotion, évènement, mise en scéne, décoration, matériel authentique, entreprise de location, installation, service." />
 	<Meta name = "dc.title" content = "Othentic: La théâtralisation de l'Espace de vente à thème." />
	<title>Othentic</title>
	<link href="styles/index.css" media="all" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" media="max_width" href="styles/mobile.css"/>
	<link rel="icon" type="image/png" href="images/logo.png"/>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>
</head>
<body>
	<h1><img src="images/othentic1.jpg" alt="Othentic"/><a href="acceuil.php"></a></h1>
	
    <div class="slider">
		<ul>
			<li><img id="ddd" src="images/othentic_g.jpg" alt="home"/></li>
			<li><img id="eee" src="images/autres 091.jpg" alt="autres 091"/></li>
			<li><img id="fff" src="images/Sol 1.jpg" alt="Sol 1"/></li>
			<li><img id="ggg" src="images/moto i.jpg" alt="moto i"/></li>
		</ul>
	</div>

	<div id="main">
		<h2>OTHENTIC : La théâtralisation des événements, la location de matériel de décoration authentique… Qui sommes-nous ?</h2>
		<p> Depuis 1998, OthentiC met en scène les opérations commerciales de la grande distribution et l’évènementiel de nombreuses manifestations.<br/>
			C’est par exemple :
		</p>
		<ul>
			<li>la décoration de la Foire aux vins d’un Hypermarché,</li>
			<li>la décoration de la Foire au gras d’un Supermarché,</li>
			<li>la décoration d’une Mairie pour Noël,</li>
			<li>la décoration de la Rentrée des classes en Galerie Commerciale,</li>
			<li>la décoration d’une Salle pour un Mariage à thème,</li>
			<li>la décoration d’un stand sur un Salon ou lors d’une Foire,</li> 
			<li>la décoration d’un Palais des Congrès,</li>
			<li>la décoration d’une Fête privée, etc.</li>
		</ul>
		<p>L’originalité de la décoration provient du matériel authentique qui crée une ambiance unique.<br/>
		OthentiC c’est la location mais aussi l’implantation de votre décoration.<br/>
		Alors n’attendez plus, contactez-nous en suivant sur l’onglet contact. </p>
	</div>		
	
	<p id="part">En Partenariat avec : <br/>
	Cliquez ici pour voir les partenaires. </p>

	<div id="footer">
		<img src="images/auchan.jpg" alt="Auchan"/>
		<img src="images/carrefour market.jpg" alt="Carrefour Market"/>
		<img src="images/carrefour.jpg" alt="Carrefour"/>
		<img src="images/cora.jpg" alt="cora"/>
		<img src="images/geantcasino.jpg" alt="Geant Casino"/>
		<img src="images/Hyper U.jpg" alt="Hyper U"/>
		<img src="images/leclerc.jpg" alt="Leclerc"/>
	</div>

	<p id="bar">_________________________________________________________________________________</p>	
	<p id="copy">Copyright WebDev &#169; 2014 Othentic Inc. Tous droits réservés.</p>
	<p id="rcs">  R.C.S. Bordeaux : A 415 392 588 - N° Siret : 415 392 588 00026 - Code APE : 748 K – N° TVA intra : FR55415392588</p>
<script type="text/javascript" src="scripts/app.js"></script>
<script type="text/javascript" src="scripts/jquery.min.js"></script>
<script type="text/javascript" src="scripts/unslider.js"></script>
<script type="text/javascript" src="scripts/unslider.min.js"></script>
</body>
</html>